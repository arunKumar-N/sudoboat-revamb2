import React, { useEffect, useState } from "react";
import styles from "./../styles/Footer.module.css";
import { TextField } from "@mui/material";
import Button from "@mui/material/Button";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
const Footer = () => {
  const [footerContent, setFooterContent] = useState();
  const getresponse = async () => {
    const contentful = require("contentful");

    const client = contentful.createClient({
      space: "6h5tctqma4tw",
      environment: "master", // defaults to 'master' if not set
      accessToken: "hPKw7_4S3nR921mUeLzueyk3lIpSX3qobifuyqwJvmk",
    });

    client
      .getEntry("ma33ek87gPolKAbLRh4Gt")
      .then((entry) => setFooterContent(entry.fields))
      .catch(console.error);
  };
  useEffect(() => {
    getresponse();
  }, []);
  return (
    <div className={styles.footerContent}>
      <div className={styles.footerHeader}>
        <div className={styles.footerTitles}>
          <div className={styles.footerTitlesTxt}>{footerContent?.title}</div>
        </div>
        <div className={styles.footerdescriptionTxt}>
          {footerContent?.description}
        </div>
        <div className={styles.emailReqField}>
          <input
            type="search"
            id="search"
            placeholder="Search"
            autoFocus={false}
            className={styles.headerSearchBar}
            style={{ paddingLeft: "20px" }}
          />
          <Button className={styles.headerSearchBarBtn} variant="contained">
            Get a Demo
          </Button>
        </div>
      </div>
      <div className={styles.border}></div>
      <div className={styles.officeDetails}>
        {footerContent?.slugs?.map((data) => {
          return (
            <>
              {data.fields?.slug && (
                <div className={styles.particularDetails}>
                  <div className={styles.particularDetailsHeader}>
                    {data.fields?.slug}
                  </div>
                  {data.fields?.slugSubMenu?.map((data) => {
                    return (
                      <>
                        <div className={styles.particularDetailsBodyTxt}>
                          <div>{data}</div>
                        </div>
                      </>
                    );
                  })}
                </div>
              )}
            </>
          );
        })}
      </div>
      <div className={styles.partnerLogos}>
        <div className={styles.partnerImageContainer}>
          {footerContent?.logos?.map((data) => {
            return (
              <>
                <img
                  src={data?.fields?.file?.url}
                  alt=""
                  className={styles.partnerImages}
                />
              </>
            );
          })}
        </div>
        <div className={styles.copyRights}>{footerContent?.copyRights}</div>
      </div>
    </div>
  );
};

Footer.propTypes = {};

export default Footer;
